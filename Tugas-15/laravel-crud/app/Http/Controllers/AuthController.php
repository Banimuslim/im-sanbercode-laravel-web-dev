<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerView(){
        return view('halaman.form');
    }

    public function daftar(request $request){
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];

        return view('halaman.welcome',['fname'=>$namadepan, 'lname'=>$namabelakang]);
    }
}
