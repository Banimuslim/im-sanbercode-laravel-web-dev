<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'homeView']);

Route::get('/register', [AuthController::class, 'registerView']);

Route::post('/welcome', [AuthController::class, 'daftar']);

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-tables');
});

// Fungsi CRUD untuk Cast
// create data
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

// read Data
Route::get('/cast', [CastController::class, 'index']);
// menampilkan data tunggal
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// update data
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);