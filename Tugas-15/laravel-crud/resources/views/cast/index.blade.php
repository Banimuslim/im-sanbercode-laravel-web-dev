@extends('layout.master')

@section('judul')
	Data Cast
@endsection

@section('isi')

	<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah</a>

	<table class="table">
		<thead>
		<tr>
			<th>No.</th>
			<th>Nama Cast</th>
			<th>Umur</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody>
		@forelse($cast as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->nama}}</td>
				<td>{{$value->umur}}</td>
				<td>
					<form action="/cast/{{$value->id}}" method="post">
						@csrf
						@method('delete')
						<a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
						<a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
						<input type="submit" value="delete" class="btn btn-danger btn-sm">
					</form>
				</td>
			</tr>
		@empty
			<tr><td>tidak ada data</td></tr>
		@endforelse
		</tbody>
	</table>
@endsection
