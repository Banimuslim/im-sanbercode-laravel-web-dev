@extends('layout.master')

@section('judul')
	Detail Cast terpilih
@endsection

@section('isi')
	<h3>{{$cast->nama}}</h3>

	<p>Usia: {{$cast->umur}} tahun</p>
	<p>Bio Cast: {{$cast->bio}}</p>
@endsection
