@extends('layout.master')

@section('judul')
	Tambah Data Cast
@endsection

@section('isi')
	<form action="/cast" method="post">
		@csrf
		<div class="form-group">	
			<label>Nama cast:</label>
			<input type="text" class="form-control" name="nama">
		</div>
		@error('nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		<div class="form-group">	
			<label>Umur:</label>
			<input type="text" class="form-control" name="umur">
		</div>
		@error('umur')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		<div class="form-group">	
			<label>Bio:</label>
			<textarea type="text" class="form-control" name="bio" rows="5"></textarea>
		</div>
		@error('bio')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror

		<input type="submit" name="submit">
	</form>
@endsection
