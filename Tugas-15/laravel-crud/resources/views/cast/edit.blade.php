@extends('layout.master')

@section('judul')
	Edit Cast terpilih
@endsection

@section('isi')
	<form action="/cast/{{$cast->id}}" method="post">
		@csrf
		@method('put')
		<div class="form-group">	
			<label>Nama cast:</label>
			<input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
		</div>
		@error('nama')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		<div class="form-group">	
			<label>Umur:</label>
			<input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
		</div>
		@error('umur')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		<div class="form-group">	
			<label>Bio:</label>
			<textarea type="text" class="form-control" name="bio" rows="5">{{$cast->bio}}</textarea>
		</div>
		@error('bio')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror

		<input type="submit" name="submit" value="Edit Data!">
	</form>
@endsection
