<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerView(){
        return view('form');
    }

    public function kirim(Request $data){
        $fname = $data['fname'];
        $lname = $data['lname'];
        $gender = $data['gender'];
        $nationality = $data['nationality'];
        $lang = $data['lang'];
        $bio = $data['bio'];

        return view('welcome',['fname'=>$fname, 'lname'=>$lname]);
    }
}
