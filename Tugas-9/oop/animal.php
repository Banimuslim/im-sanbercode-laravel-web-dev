<?php  

	class Animal {
		public $legs = 4;
		public $cold_blooded = 'no';
		public $name;

		public function __construct($nama){
			$this->name = $nama;
		}

		public function tampilHewan(){
			echo "Name: ".$this->name."<br>";
			echo "legs: ".$this->legs."<br>";
			echo "cold_blooded: ".$this->cold_blooded."<br>";
		}
	}


?>