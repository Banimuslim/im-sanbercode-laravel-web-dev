<?php  

	require_once 'animal.php';


	echo '<h3>RELEASE 0</h3>';

	$sheep = new Animal("shaun");

	echo $sheep->name; // "shaun"
	echo '<br>';
	echo $sheep->legs; // 4
	echo '<br>';
	echo $sheep->cold_blooded; // "no"

	// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


	echo '<h3>RELEASE 1</h3>';

	require_once 'frog.php';
	require_once 'ape.php';

	$sungokong = new Ape("kera sakti");
	$sungokong->yell() ;// "Auooo"

	echo '<br>';
	$kodok = new Frog("buduk");
	$kodok->jump() ; // "hop hop"
	echo '<br>';

	echo '<h3>Ouput</h3>';
	$sheep->tampilHewan();
	echo '<br>';
	$kodok->tampilHewan();
	echo '<br>';
	$sungokong->tampilHewan();


?>