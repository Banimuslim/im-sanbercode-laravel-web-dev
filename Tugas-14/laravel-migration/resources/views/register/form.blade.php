@extends('layout.master')

@section('judul')
	Buat Account Baru!
@endsection

@section('content')
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
		@csrf
		<label for="fname">First Name:</label> <br>
		<input type="text" name="fname" id="fname"><br><br>

		<label for="lname">Last Name:</label><br>
		<input type="text" name="lname" id="lname"><br><br>

		<label>Gender</label><br>
		<input type="radio" name="gender" id="male" value="Male">
		<label for="male">Male</label><br>

		<input type="radio" name="gender" id="female" value="Female">
		<label for="female">Female</label><br>

		<input type="radio" name="gender" id="other" value="Other">
		<label for="other">Other</label><br><br>


		<label for="nationality">Nationality</label><br>
		<select name="nationality" id="nationality">
			<option value="Indonesian">Indonesian</option>
			<option value="Arabian">Arabian</option>
			<option value="Malaysian">Malaysian</option>
		</select><br><br>

		<label>Language Spoken:</label><br>
		<input type="checkbox" name="lang" id="indo" value="Bahasa Indonesia">
		<label for="indo">Bahasa Indonesia</label><br>

		<input type="checkbox" name="lang" id="english" value="English">
		<label for="english">English</label><br>

		<input type="checkbox" name="lang" id="llain" value="Other">
		<label for="llain">Other</label><br><br>

		<label for="bio">Bio:</label><br>
		<textarea name="bio" id="bio" rows="10" cols="30"></textarea><br><br>

		<button type="submit" name="submit">Sign Up</button>
	</form>
@endsection
